//
//  StatusView.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-14.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import UIKit

/// A view for showing a loading indicator and message label when loading online content.
public class StatusView: UIView {

    /// Create a new `StatusView` instance.
    public class func createInstance() -> StatusView {
        return Bundle(identifier: "ca.joshuabuxton.Core")!.loadNibNamed("StatusView", owner: self, options: nil)!.first as! StatusView;
    }

    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var messageLabel: UILabel!

    public override var isHidden: Bool {
        didSet {
            if isHidden {
                self.loadingIndicator.stopAnimating();
            }
        }
    }

    // MARK: - Status display

    /// Show the loading indicator.
    public func showLoading() {
        loadingIndicator.startAnimating();

        UIView.animate(withDuration: 0.5, animations: {
            self.loadingIndicator.alpha = 1;
            self.messageLabel.alpha = 0;
        });
    }

    /// Show a message in the message label.
    public func showMessage(_ message: String) {
        self.messageLabel.text = message;

        UIView.animate(withDuration: 0.5, animations: {
            self.loadingIndicator.alpha = 0;
            self.messageLabel.alpha = 1;
        }, completion: { completed in
            if completed {
                self.loadingIndicator.stopAnimating();
            }
        });
    }
}
