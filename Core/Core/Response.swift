//
//  Response.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

public enum Response<T: Resource> {
    case success(value: T);
    case failure(error: Error);

    public func unwrap() throws -> T {
        switch self {
            case .success(let value):
                return value;
            case .failure(let error):
                throw error;
        }
    }
}
