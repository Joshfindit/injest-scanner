//
//  Access.swift
//  Core
//
//  Created by Seth and Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

extension Notification.Name {
    public static let networkConnected = Notification.Name("networkConnected");
}

public class Access {

    // testing: http://159.89.120.21:48888
    // production: http://192.168.2.100:48888
    private let server = URL(string: "http://192.168.2.100:48888")!;
    private let webSession = "edc4d3ec-e005-44c2-8072-2aedfeaa0dcc"; //todo: proper login, currently uses static admin key

    /// The global `Access` instance to share within an application.
    public static let shared = Access();

    /// The current network state for reachability to the host server.
    private var networkState: NetworkState = .online {
        didSet {
            if networkState == .online {
                NotificationCenter.default.post(name: .networkConnected, object: nil);
            }
        }
    }

    /// Initialize this access instance.
    ///
    /// Must be called before using this instance.
    public func initialize() throws {
        let reachability = Reachability(hostname: server.host!)!;

        reachability.whenReachable = { reachability in
            switch reachability.connection {
                case .wifi, .cellular:
                    self.networkState = .online;
                default:
                    break;
            }
        }

        reachability.whenUnreachable = { _ in
            self.networkState = .offline;
        }

        try reachability.startNotifier();
    }

    // MARK: - Requests

    // MARK: - GET

    /// Get a listing of all the unreceived invoices with the given callback.
    public func getInvoices(_ callback: @escaping (Response<JSONArrayResource<Invoice>>) -> Void) -> URLSessionDataTask? {
        return get(url: URL(string: "/suppliershipmentinvoices/unreceivedlist.json", relativeTo: server)!.absoluteURL, callback);
    }

    /// Get the category for the given number, with the given callback.
    public func getCategory(number: Int, _ callback: @escaping (Response<Category>) -> Void) -> URLSessionDataTask? {
        return get(url: URL(string: "/numberedcategories/\(number).json", relativeTo: server)!.absoluteURL, callback);
    }

    /// Get a bolt with the given UUID and the given callback.
    public func getBolt(uuid: String, _ callback: @escaping (Response<Bolt>) -> Void) -> URLSessionDataTask? {
        return get(url: URL(string: "/bolts/\(uuid).json", relativeTo: server)!.absoluteURL, callback);
    }

    /// Get an image at the given path, relative to the server, with the given callback
    public func getImage(url: URL, _ callback: @escaping (Response<ImageResource>) -> Void) -> URLSessionDataTask? {
        return get(url: URL(string: url.absoluteString, relativeTo: server)!, callback);
    }

    // MARK: - POST

    /// Change the image of the given bolt to the given image, with the given callback.
    public func postBoltImage(_ image: UIImage, for bolt: Bolt, _ callback: @escaping (Response<Bolt>) -> Void) -> URLSessionDataTask? {
        let encodedImage = UIImagePNGRepresentation(image.resized(toFitWidth: 1024)!)!.base64EncodedString();

        let json = JSON([
            "imagetoingest": [
                "rough_photo_of_uuid": bolt.uuid,
                "base64encodedimage": encodedImage,
                "metadata": [
                    "filename": "\(Date().timeIntervalSince1970).png",
                ],
            ],
        ]);

        return post(url: URL(string: "/imageuploadbybase64", relativeTo: server)!, body: json, callback);
    }

    /// Post the given bolt to the server with the given callback.
    public func postBolt(_ bolt: Bolt, _ callback: @escaping (Response<Bolt>) -> Void) -> URLSessionDataTask? {
        return post(url: URL(string: "/bolts", relativeTo: server)!, body: bolt.encode(), callback);
    }

    // MARK: - DELETE

    /// Delete a bolt with the given UUID. API endpoint: `delete '/bolts/:uuid.json'`
    public func deleteBolt(uuid: String, _ callback: @escaping (Response<Message>) -> Void) -> URLSessionDataTask? {
        return delete(url: URL(string: "/bolts/\(uuid).json", relativeTo: server)!.absoluteURL, callback);
    }

    // MARK: - Networking

    private func get<T: Resource>(url: URL, _ callback: @escaping (Response<T>) -> Void) -> URLSessionDataTask? {
        return makeRequest(URLRequest(url: url), callback: callback);
    }

    private func post<T: Resource>(url: URL, body: JSON, _ callback: @escaping (Response<T>) -> Void) -> URLSessionDataTask? {
        var request = URLRequest(url: url);

        request.setValue("application/json", forHTTPHeaderField: "Content-Type");
        request.httpMethod = "POST";

        do {
            request.httpBody = try body.rawData();

            return makeRequest(request, callback: callback);
        }
        catch let e {
            print("Error encoding POST body JSON: \(e)");
            callback(.failure(error: e));
            return nil;
        }
    }

    private func delete<T: Resource>(url: URL, _ callback: @escaping (Response<T>) -> Void) -> URLSessionDataTask? {
        var request = URLRequest(url: url);
        request.httpMethod = "DELETE";

        return makeRequest(request, callback: callback);
    }


    /// Authenticate and perform the given `URLRequest` with the given callback.
    private func makeRequest<T: Resource>(_ request: URLRequest, callback: @escaping (Response<T>) -> Void) -> URLSessionDataTask? {
        if networkState == .online {
            var authorizedRequest = request;

            authorizedRequest.setValue("Token token=\(webSession)", forHTTPHeaderField: "Authorization");

            let task = URLSession.shared.dataTask(with: authorizedRequest) { data, response, error in
                if let e = error as NSError? {
                    switch e.domain {
                        case NSURLErrorDomain:
                            switch e.code {
                                case NSURLErrorNotConnectedToInternet:
                                    callback(.failure(error: AccessError.offline));
                                    return;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }

                    callback(.failure(error: e));
                    return;
                }

                if let d = data {
                    if let r = response as? HTTPURLResponse {
                        let contentType = r.allHeaderFields["Content-Type"] as? String;

                        if r.statusCode < 200 || r.statusCode >= 300 {
                            let json = JSON(data: d);

                            callback(.failure(error: AccessError.generic(status: json["status"].int ?? r.statusCode, message: json["message"].string ?? "")));
                            return;
                        }
                        else if contentType != "application/json;charset=utf-8" && !(contentType?.hasPrefix("image/") ?? false) && contentType != nil {
                            // when the api is in debug mode it will return html pages to show detailed error info, so the content-type needs to be validated so it is not parsed
                            callback(.failure(error: AccessError.invalidContentType(type: contentType!)));
                            return;
                        }
                    }

                    callback(.success(value: T(data: d)));
                }
            }

            task.resume();
            return task;
        }
        else {
            callback(.failure(error: AccessError.offline));
            return nil;
        }
    }

    // MARK: - Enums

    /// The state for the reachability to the host server.
    private enum NetworkState {
        /// Unable to connect to the host server.
        case offline

        /// Able to connect to the host server.
        case online
    }

    /// Various common errors that this class can throw when making requests.
    public enum AccessError: LocalizedError {
        /// Unable to reach the host server.
        case offline

        /// The `Content-Type` header was not a supported type.
        case invalidContentType(type: String);

        /// A generic error for unhandled HTTP status code errors.
        case generic(status: Int, message: String);

        public var errorDescription: String? {
            switch self {
                case .offline:
                    return "The Internet connection appears to be offline.";
                case .invalidContentType(let type):
                    return "Invalid content type received (\(type)).";
                case .generic(let status, let message):
                    return "\(message) (\(status))";
            }
        }
    }
}
