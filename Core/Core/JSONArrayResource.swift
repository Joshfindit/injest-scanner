//
//  ArrayResponse.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

/// A `JSONResource` that parses from a root array object.
public class JSONArrayResource<T: JSONResource>: JSONResource {

    /// The items that this resource parsed.
    public let items: [T]?

    public required init(json: JSON) {
        self.items = json.array?.map({ T(json: $0) });

        super.init(json: json);
    }
}
