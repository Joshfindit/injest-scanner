//
//  ImageResource.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-14.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

public struct ImageResource: Resource {

    /// The image for this resource.
    public let image: UIImage?

    public init(data: Data) {
        self.image = UIImage(data: data);
    }
}
