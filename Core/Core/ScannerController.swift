//
//  ScannerController.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import UIKit
import AVFoundation

/// A view controller for scanning `Bolt`s from QR codes.
open class ScannerController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    /// The host that proper QR codes will be on.
    private let codeHost = "canadianfabrics.ca";

    private var captureSession: AVCaptureSession?
    private var previewLayer: AVCaptureVideoPreviewLayer?
    private var codeFrameView: UIView?

    /// Whether or not this controllers should try to hide the status bar when it is frontmost.
    open var hidesStatusBar: Bool {
        return true;
    }

    open override var prefersStatusBarHidden: Bool {
        return hidesStatusBar ? captureSession?.isRunning ?? false : false;
    }

//    open override func prefersHomeIndicatorAutoHidden() -> Bool {
//        return true;
//    }

    open override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide;
    }

    open override func viewDidLoad() {
        super.viewDidLoad();

        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo);

        do {
            let input = try AVCaptureDeviceInput(device: captureDevice);

            captureSession = AVCaptureSession();
            captureSession!.addInput(input);

            let metadataOutput = AVCaptureMetadataOutput();
            captureSession!.addOutput(metadataOutput);

            metadataOutput.setMetadataObjectsDelegate(self, queue: .main);
            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode];

            if let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession) {
                previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                previewLayer.frame = view.layer.bounds;

                view.layer.addSublayer(previewLayer);
                self.previewLayer = previewLayer;

                codeFrameView = UIView();
                codeFrameView!.layer.borderWidth = 2;
                view.addSubview(codeFrameView!);
                view.bringSubview(toFront: codeFrameView!);
            }
            else {
                print("Unable to create preview layer");
            }
        }
        catch let e {
            if let ave = e as? AVError {
                switch ave.code {
                    case .applicationIsNotAuthorizedToUseDevice:
                        // unable to use camera
                        return;
                    default:
                        break;
                }
            }

            // todo: show errors in the interface

            print("Unexpected error creating capture input: \(e)");
        }
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);

        hideCodeFrame();
        captureSession?.startRunning();
    }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);

        updateStatusBar();

        #if (arch(i386) || arch(x86_64)) && os(iOS)
            // automatically show the info for 09-013-66 if in the simulator as there is no camera
            // b4990577-cdb4-4986-9743-c11bd6014804 / 67
            showInfo(CodeInfo(uuid: NSUUID().uuidString, categoryNumber: 80));
        #endif
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);

        updateStatusBar();
    }

    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews();

        // resize the preview layer and rotate the camera properly when this controller is resized

        if let preview = previewLayer, let connection = previewLayer?.connection {
            if connection.isVideoOrientationSupported {
                let orientation = AVCaptureVideoOrientation(rawValue: UIApplication.shared.statusBarOrientation.rawValue)!;

                preview.frame = self.view.bounds;
                connection.videoOrientation = orientation;
            }
        }
    }

    // MARK: - Status bar

    private func updateStatusBar() {
        UIView.animate(withDuration: 0.5, animations: {
            self.setNeedsStatusBarAppearanceUpdate();
        });
    }

    // MARK: - Data display

    /// Called when a code is scanned and info is parsed from it.
    open func showInfo(_ info: CodeInfo) {
    }

    // MARK: - Metadata object delegate

    public func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        if metadataObjects == nil || metadataObjects.isEmpty {
            // no code found, so hide the frame
            hideCodeFrame();
            return;
        }

        if let metadata = metadataObjects.first as? AVMetadataMachineReadableCodeObject, let preview = previewLayer {
            if metadata.type == AVMetadataObjectTypeQRCode {
                let code = preview.transformedMetadataObject(for: metadata) as! AVMetadataMachineReadableCodeObject;

                if let url = URL(string: code.stringValue) {
                    // guarantee there is a root, category, and uuid component
                    // Example: `["/", "09", "1CF02CDA-8593-4711-9F28-9702F3B757FC"]`
                    if url.pathComponents.count == 4 && url.host == codeHost {
                        // show the code frame
                        showCodeFrame(rect: code.bounds, isValid: true);

                        // play a sound to tell the user that the code is valid
                        AudioServicesPlaySystemSound(1054);

                        // show the parsed info
                        showInfo(CodeInfo(uuid: url.pathComponents[3], categoryNumber: Int(url.pathComponents[2])!));

                        // stop the capture session
                        captureSession?.stopRunning();

                        return;
                    }
                }

                // show that the code is invalid
                showCodeFrame(rect: code.bounds, isValid: false);
            }
        }
    }

    // MARK: - Code frame

    private func showCodeFrame(rect: CGRect, isValid: Bool) {
        codeFrameView?.frame = rect;

        if isValid {
            codeFrameView?.layer.borderColor = UIColor.green.cgColor;
        }
        else {
            codeFrameView?.layer.borderColor = UIColor.red.cgColor;
        }
    }

    private func hideCodeFrame() {
        codeFrameView?.frame = .zero;
    }

    // MARK: - Structs

    /// Information parsed from a QR code.
    public struct CodeInfo {
        /// The UUID on the scanned code.
        public let uuid: String;

        /// The category number on the scanned code.
        public let categoryNumber: Int;
    }
}
