//
//  StringExtensions.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-24.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

extension Optional where Wrapped == String {
    /// Whether or not this string is either empty or nil.
    public var isNilOrEmpty: Bool {
        switch self {
            case .none:
                return true;
            case .some(let value):
                return value.isEmpty;
        }
    }

    /// Returns nil if this string is empty, else returns this string.
    public var nilIfEmpty: String? {
        switch self {
            case .none:
                return nil;
            case .some(let value):
                return value.isEmpty ? nil : value;
        }
    }
}
