//
//  OnlineController.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-14.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

/// A utility class for handling loading content from online.
public class OnlineController<T: Resource> {

    /// Whether or not this controller has already been started.
    private var isStarted = false;

    /// The current `LoadingState` of this controller.
    private var loadingState: LoadingState = .unloaded;

    /// The last `URLSessionDataTask` used to load content for this controller.
    private var task: URLSessionDataTask?

    /// The `StatusView` for this controller to manage.
    public var status: StatusView?

    /// The method to prepare to load content for this controller.
    ///
    /// E.g. `OnlineImageView` clears it's image here.
    public var prepare: (() -> Void)?

    /// The method to load the content for this controller.
    ///
    /// Called with a callback method that should be passed to a `Access` request method.
    /// Should return the `URLSessionDataTask` returned by the called `Access` request method.
    public var loadContent: ((@escaping (Response<T>) -> Void) -> URLSessionDataTask?)?

    /// The method to display successfully loaded content for this controller.
    ///
    /// Called with the successfully loaded content.
    public var showContent: ((T) -> Void)?

    /// The optional method to call when there is an error while retrieving this controller's content.
    ///
    /// Called with the error that occured, should return whether or not the handling was successful.
    /// When returning true the status is unaffected and this controller acts as though it loaded normally.
    /// When returning false the default handling is used as a fallback, updating the status.
    public var errorHandler: ((Error) -> Bool)?

    public init() {
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: .networkConnected, object: nil);
    }

    // MARK: - Networking

    /// Connect and start trying to load the content of this controller.
    ///
    /// If content was already loaded, then it is reloaded.
    @objc public func start() {
        if !isStarted {
            NotificationCenter.default.addObserver(self, selector: #selector(networkConnected), name: .networkConnected, object: nil);
        }
        else {
            self.loadingState = .unloaded;
        }

        load();

        isStarted = true;
    }

    /// Attempt to load the content for this controller.
    private func load() {
        if loadingState == .unloaded, let l = loadContent, let s = showContent {
            self.task?.cancel();
            self.loadingState = .loading;
            self.status?.showLoading();

            prepare?();

            task = l({ response in
                do {
                    let content = try response.unwrap();

                    DispatchQueue.main.async {
                        s(content);
                    }

                    self.loadingState = .loaded;
                }
                catch let e {
                    DispatchQueue.main.async {
                        if self.errorHandler?(e) ?? false {
                            self.loadingState = .loaded;
                        }
                        else {
                            self.loadingState = .unloaded;

                            let ne = e as NSError;
                            self.status?.showMessage(ne.localizedRecoverySuggestion ?? ne.localizedDescription);
                        }
                    }
                }
            });
        }
    }

    @objc private func networkConnected() {
        // try to load again when the network is reconnected and the bolt isnt loaded
        // this handles cases where the user is not connected to wifi but reconnects while this controller loaded
        if loadingState == .unloaded {
            load();
        }
    }

    // MARK: - Enums

    /// The different loading states this controller can be in.
    private enum LoadingState {
        /// The content has either not been loaded or has failed loading.
        case unloaded

        /// The content is currently loading.
        case loading

        /// The content has been successfully loaded.
        case loaded
    }
}
