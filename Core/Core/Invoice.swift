//
//  Invoice.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

/// A supplier shipment invoice.
public class Invoice: JSONResource {
    /// The UUID of this invoice.
    public let uuid: String;

    /// The name of this invoice.
    public let name: String;

    /// The human-identifiable title of the invoice: Name + Date in YYYY-MM-DD
    public let title: String;

    /// The date that the supplier shipped out this invoice.
    public let date: Date?;

    /// The lines of this invoice.
    public let lines: [Line];

    public required init(json: JSON) {
        let object = json["suppliershipmentinvoice"];

        self.uuid = object["uuid"].stringValue;
        self.name = object["name"].stringValue;

        if let d = object["invoice_date"].string {
            let dateFormatter = DateFormatter();
            dateFormatter.dateFormat = "yyyy-MM-dd";

            self.date = dateFormatter.date(from: d);
        }
        else {
            self.date = nil;
        }

        if let invoiceDate = self.date.getYYYYMMDD() {
            self.title = self.name + " (" + invoiceDate + ")";   // TODO
        } else {
            self.title = self.name + " (Unspecified Date)";
        }

        self.lines = (object["lines"].array ?? []).map({ Line(json: $0) });

        super.init(json: json);
    }

    /// An invoice line.
    public class Line: JSONResource {
        /// The UUID of this invoice line.
        public let uuid: String;

        /// The manufacturer of this invoice line's item.
        public let manufacturer: String?

        /// A description of this invoice line's item.
        public let description: String;

        /// The cost of this invoice line's item.
        public let landedCost: Double;

        /// The selling price of this invoice line's item.
        public let sellingPrice: Double;

        public required init(json: JSON) {
            self.uuid = json["uuid"].stringValue;
            self.manufacturer = json["manufacturer"].string;
            self.description = json["description"].stringValue;
            self.landedCost = json["landed_cost"].doubleValue;
            self.sellingPrice = json["selling_price"].doubleValue;

            super.init(json: json);
        }
    }
}
