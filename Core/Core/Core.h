//
//  Core.h
//  Core
//
//  Created by Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT double CoreVersionNumber;
FOUNDATION_EXPORT const unsigned char CoreVersionString[];
