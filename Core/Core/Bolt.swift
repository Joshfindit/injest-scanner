//
//  Bolt.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

/// A bolt of fabric.
public class Bolt: JSONResource, JSONEncodable {
    /// The UUID of this bolt.
    public let uuid: String;

    /// The assigned SKU of this bolt.
    public let sku: String?

    /// The amount of this bolt that are in stock
    public let stockedAmount: Int?;

    /// The supplier of this bolt.
    public let supplier: String;

    /// The length of this bolt in meters.
    public let length: NSDecimalNumber;

    /// The manufacturer of this bolt.
    public let manufacturer: String;

    /// A description of this bolt.
    public let description: String?

    /// The original cost of this bolt in CAD.
    public let originalCost: NSDecimalNumber;

    /// The selling price of this bolt in CAD.
    public let sellingPrice: NSDecimalNumber;

    /// The landed cost of this bolt in CAD.
    public let landedCost: NSDecimalNumber;

    /// The URL to a photo of this bolt.
    public let photo: URL;

    /// The URL to a thumbnail of `photo`.
    public let thumbnail: URL;

    /// The URL to a higher resolution thumbnail of `photo`.
    public let mediumThumbnail: URL;

    /// The conceptual tags of this bolt in a string.
    public let conceptualTags: String;

    /// The category tracking tags of this bolt in a string.
    public let categoryTrackingTags: String;

    /// The categories this bolt is in.
    public let categories: [String];

    /// The date and time that this bolt was ingested at.
    public let ingestDate: Date;

    /// The UUID of the invoice line this bolt is populated from.
    public let invoiceLineUUID: String?

    /// The title of the invoice line this bolt is populated from. Exposed as "populated_from_supplier_shipment_invoice_line_title": "Red Rooster, 210-70"
    public let invoiceLineTitle: String?

    /// The UUID of the *invoice* this bolt is populated from.
    public let invoiceUUID: String?

    /// The title of the *invoice* this bolt is populated from. Exposed as "populated_from_supplier_shipment_invoice_name": "Choice Fabrics  110015 "
    public let invoiceTitle: String?

    /// The date of the Invoice this bolt is populated from. Exposed as "populated_from_supplier_shipment_invoice_date": "2018-02-15"
    public let invoiceDate: String?



    /// The category this bolt is in.
    public let categoryNumber: Int?

    public required init(json: JSON) {
        let object = json["bolt"];
        let currencyFormatter = NumberFormatter();

        currencyFormatter.maximumFractionDigits = 2;

        self.uuid = object["uuid"].stringValue;
        self.sku = object["assigned_sku"].stringValue;
        self.stockedAmount = object["amount_in_stock"].int;
        self.supplier = object["supplier"].stringValue;
        self.length = NSDecimalNumber(string: object["length_in_meters"].stringValue);
        self.manufacturer = object["manufacturer"].stringValue;
        self.description = object["description"].string;
        self.originalCost = NSDecimalNumber(string: object["original_cost"].stringValue);
        self.sellingPrice = NSDecimalNumber(string: object["selling_price"].stringValue);
        self.landedCost = NSDecimalNumber(string: object["landed_cost"].stringValue);
        self.photo = URL(string: object["api_photo_url"].stringValue)!;
        self.thumbnail = URL(string: object["api_photo_thumbnail_url"].stringValue)!;
        self.mediumThumbnail = URL(string: object["api_photo_medium_thumbnail_url"].stringValue)!;
        self.conceptualTags = object["conceptual_tags"].stringValue;
        self.categoryTrackingTags = object["category_tracking_tags"].stringValue;
        self.categories = object["assigned_categories"].arrayValue.map({ $0.stringValue });
        self.ingestDate = Date(timeIntervalSince1970: Double(object["unixtimeIngestedAt"].stringValue)!);
        self.invoiceLineUUID = object["populated_from_supplier_shipment_invoice_line_uuid"].string;
        self.invoiceLineTitle = object["populated_from_supplier_shipment_invoice_line_title"].string;
        self.invoiceUUID = object["populated_from_supplier_shipment_invoice_uuid"].string;
        self.invoiceTitle = object["populated_from_supplier_shipment_invoice_name"].string;
        self.invoiceDate = object["populated_from_supplier_shipment_invoice_date"].string;

        self.categoryNumber = nil;

        super.init(json: json);
    }

    public init(uuid: String, sku: String?, length: NSDecimalNumber, manufacturer: String, ingestDate: Date, invoiceLineUUID: String, categoryNumber: Int) {
        self.uuid = uuid;
        self.sku = sku;
        self.stockedAmount = 0;
        self.supplier = "";
        self.length = length;
        self.manufacturer = manufacturer;
        self.description = "";
        self.originalCost = 0;
        self.sellingPrice = 0;
        self.landedCost = 0;
        self.photo = URL(string: "/no_photo_available.png")!;
        self.thumbnail = self.photo;
        self.mediumThumbnail = self.photo;
        self.conceptualTags = "";
        self.categoryTrackingTags = "";
        self.categories = [];
        self.ingestDate = ingestDate;
        self.invoiceLineTitle = nil;
        self.invoiceLineUUID = invoiceLineUUID;
        self.invoiceTitle = nil;
        self.invoiceDate = nil;
        self.invoiceUUID = nil;
        self.categoryNumber = categoryNumber;

        super.init(json: JSON(parseJSON: ""));
    }

    func encode() -> JSON {
        let dateFormatter = NumberFormatter();
        dateFormatter.numberStyle = .decimal;
        dateFormatter.usesGroupingSeparator = false;
        dateFormatter.minimumFractionDigits = 3;
        dateFormatter.maximumFractionDigits = 3;

        let data: [String: Any?] = [
            "uuid": uuid,
            "unixtimeIngestedAt": dateFormatter.string(from: NSNumber(value: ingestDate.timeIntervalSince1970)),
            "manufacturer": manufacturer,
            "length_in_meters": length,
            "assigned_sku": sku,
            "category_number": categoryNumber,
            "populated_from_supplier_shipment_invoice_line_uuid": invoiceLineUUID,
        ];

        return JSON(data);
    }
}
