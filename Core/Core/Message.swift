//
//  Message.swift
//  Core
//
//  Created by Josh on 2019-05-01.
//  Copyright © 2019 Josh Buxton. All rights reserved.
//

import Foundation

public class Message: JSONResource {
    public let status: Int;
    public let message: String;

    public required init(json: JSON) {
        self.status = json["status"].intValue;
        self.message = json["message"].stringValue;

        super.init(json: json);
    }
}
