//
//  JSONEncodable.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-24.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

protocol JSONEncodable {
    /// Encode this object into a JSON object.
    func encode() -> JSON;
}
