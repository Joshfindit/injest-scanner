//
//  Resource.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-14.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

public protocol Resource {
    init(data: Data);
}
