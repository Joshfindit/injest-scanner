//
//  DateExtensions.swift
//  Core
//
//  Created by Josh on 2019-06-10.
//  Copyright © 2019 Josh Buxton. All rights reserved.
//

import Foundation

extension Optional where Wrapped == Date {
    // Return the Date as the optional string in format "YYYY-MM-DD"
    public func getYYYYMMDD() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let selfData = self {
            return dateFormatter.string(from: selfData)
        } else {
            return nil
        }
    }
}
