//
//  Category.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-24.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

public class Category: JSONResource {

    /// The number of this category.
    public let number: Int;

    /// The name of this category.
    public let name: String;

    public required init(json: JSON) {
        let object = json["numbered_category"];

        self.number = object["index_number"].intValue;
        self.name = object["name"].stringValue;

        super.init(json: json);
    }
}
