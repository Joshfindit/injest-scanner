//
//  OnlineImageView.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-14.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import UIKit

/// A `UIImageView` that loads online images.
public class OnlineImageView: UIImageView {

    private let online = OnlineController<ImageResource>();

    /// The URL for this view to load it's image from, relative to the server.
    ///
    /// Should only be set once, loads on set.
    public var url: URL? {
        didSet {
            if url != nil {
                online.start();
            }
        }
    }

    public override func awakeFromNib() {
        super.awakeFromNib();

        // setup the online controller
        online.prepare = { [unowned self] in
            self.showImage(nil);
        }

        online.loadContent = { [unowned self] callback in
            if let u = self.url {
                return Access.shared.getImage(url: u, callback);
            }

            return nil;
        }

        online.showContent = { [unowned self] resource in
            self.showImage(resource.image);
        }
    }

    // MARK: - Image display

    private func showImage(_ image: UIImage?) {
        UIView.transition(with: self, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.image = image;
        }, completion: nil);
    }
}
