//
//  ImageExtensions.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-24.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

extension UIImage {
    /// Get a resized copy of this image aspect fitted into the given width.
    func resized(toFitWidth width: CGFloat) -> UIImage? {
        // calculate the new size
        let newSize = CGSize(width: width, height: width * (size.height / size.width));

        // draw the image in the new size
        UIGraphicsBeginImageContext(newSize);

        defer {
            UIGraphicsEndImageContext();
        }

        self.draw(in: CGRect(origin: .zero, size: newSize));

        // return the resized image
        return UIGraphicsGetImageFromCurrentImageContext();
    }
}
