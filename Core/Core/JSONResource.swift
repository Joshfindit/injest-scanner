//
//  JSONDecodable.swift
//  Core
//
//  Created by Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation

/// A `Resource` that loads from JSON.
public class JSONResource: Resource {

    public required convenience init(data: Data) {
        self.init(json: JSON(data: data));
    }

    public required init(json: JSON) {
    }
}
