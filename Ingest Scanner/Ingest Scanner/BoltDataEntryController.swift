//
//  BoltDataEntryController.swift
//  Ingest Scanner
//
//  Created by Josh Buxton on 2018-10-24.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import UIKit
import Core

class BoltDataEntryController: UITableViewController {

    private let online = OnlineController<Core.Category>();

    private var status: StatusView!
    private var info: ScannerInfo!

    @IBOutlet private weak var saveButton: UIBarButtonItem!

    @IBOutlet private weak var skuField: UITextField!
    @IBOutlet private weak var categoryField: UITextField!
    @IBOutlet private weak var colourField: UITextField!
    @IBOutlet private weak var manufacturerField: UITextField!
    @IBOutlet private weak var lengthField: UITextField!

    @IBAction func onSave(_ sender: Any) {
        let bolt = Bolt(uuid: info.code.uuid, sku: skuField.text.nilIfEmpty, length: NSDecimalNumber(string: lengthField.text!), manufacturer: manufacturerField.text!, ingestDate: Date(), invoiceLineUUID: info.line.uuid, categoryNumber: info.code.categoryNumber);

        // disable the save button while the request is running
        saveButton.isEnabled = false;

        _ = Access.shared.postBolt(bolt) { response in
            do {
                let newBolt = try response.unwrap();

                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Added bolt", message: newBolt.sku ?? "Unknown SKU", preferredStyle: .alert);

                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [unowned self] _ in
                        self.navigationController?.popViewController(animated: true);
                    }));

                    self.present(alert, animated: true, completion: nil);
                }
            }
            catch let e {
                print("Error posting bolt: \(e)");

                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Failed to add bolt", message: e.localizedDescription, preferredStyle: .alert);
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil));

                    self.present(alert, animated: true, completion: nil);
                }
            }

            self.saveButton.isEnabled = true;
        }
    }

    @IBAction func fieldChanged(_ sender: Any) {
        validateFields();
    }

    override func viewDidLoad() {
        super.viewDidLoad();

        self.status = StatusView.createInstance();
        tableView.backgroundView = status;
        status.layer.zPosition = 10;
        online.status = status;

        // setup the online controller
        online.loadContent = { [unowned self] callback in
            self.showStatus();
            return Access.shared.getCategory(number: self.info.code.categoryNumber, callback);
        }

        online.showContent = { [unowned self] category in
            self.hideStatus();

            self.categoryField.text = String(category.number);
            self.colourField.text = category.name;
            self.manufacturerField.text = self.info.line.manufacturer;
        }

        online.errorHandler = { error in
            if let ae = error as? Access.AccessError {
                switch ae {
                    case .generic(let status, _):
                        if status == 404 {
                            // ignore if the category returned a 404
                            self.hideStatus();

                            self.categoryField.text = String(self.info.code.categoryNumber);
                            self.colourField.text = "";
                            self.manufacturerField.text = self.info.line.manufacturer;

                            return true;
                        }
                        break;
                    default:
                        break;
                }
            }

            return false;
        }

        // call the initial field validation
        validateFields();

        // start the online controller
        online.start();
    }

    // MARK: - Data display

    /// Set the `ScannerInfo` this controller is entering data for.
    func enterData(for info: ScannerInfo) {
        self.info = info;
    }

    // MARK: - Validation

    private func validateFields() {
        let length = Double(lengthField.text ?? "");

        if length != nil && length! > 0 && length! <= 50 {
            saveButton.isEnabled = !manufacturerField.text.isNilOrEmpty && !lengthField.text.isNilOrEmpty;
        }
        else {
            saveButton.isEnabled = false;
        }
    }

    // MARK: - Status view

    /// Animate in the status view.
    private func showStatus() {
        UIView.animate(withDuration: 0.5, animations: {
            self.status.alpha = 1;
        });
    }

    /// Animate out the status view.
    private func hideStatus() {
        UIView.animate(withDuration: 0.5, animations: {
            self.status.alpha = 0;
        }, completion: { completed in
            if completed {
                self.status.isHidden = true;
            }
        });
    }
}
