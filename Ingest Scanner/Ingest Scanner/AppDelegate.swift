//
//  AppDelegate.swift
//  Ingest Scanner
//
//  Created by Seth and osh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import UIKit
import Core
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        do {
            try Access.shared.initialize()
        }
        catch {
            print("Unable to initialize Access.shared: \(error)");
        }

        FirebaseApp.configure()
        return true
    }
}
