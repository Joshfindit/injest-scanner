//
//  ScannerInfo.swift
//  Ingest Scanner
//
//  Created by Josh Buxton on 2018-10-24.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Foundation
import Core

// todo: better name?
struct ScannerInfo {
    /// The `Invoice.Line` associated with this info.
    let line: Invoice.Line;

    /// The `CodeInfo` associated with this info.
    let code: ScannerController.CodeInfo;
}
