//
//  BoltScannerController.swift
//  Ingest Scanner
//
//  Created by Josh Buxton on 2018-10-24.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import UIKit
import Core

class BoltScannerController: ScannerController {

    /// The `Invoice.Line` this controller is scanning codes for.
    private var line: Invoice.Line!

    /// The last `CodeInfo` this controller scanned.
    private var info: CodeInfo!

    internal override var hidesStatusBar: Bool {
        return false;
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EntrySegue" {
            if let c = segue.destination as? BoltDataEntryController {
                c.enterData(for: ScannerInfo(line: line, code: info));
            }
        }
    }

    // MARK: - Data display

    /// Set the `Invoice.Line` this controller is scanning a code for.
    func scanForLine(_ line: Invoice.Line) {
        self.line = line;
    }

    internal override func showInfo(_ info: CodeInfo) {
        self.info = info;
        self.performSegue(withIdentifier: "EntrySegue", sender: nil);
    }
}
