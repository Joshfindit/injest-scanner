//
//  LinesController.swift
//  Ingest Scanner
//
//  Created by Josh Buxton on 2018-10-24.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import UIKit
import Core

class LinesController: UITableViewController {

    /// The currently displayed invoice lines in this controller.
    private var items: [Invoice.Line] = [] {
        didSet {
            tableView.insertRows(at: (0..<items.count).map({ IndexPath(row: $0, section: 0) }), with: .automatic);
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ScannerSegue" {
            if let c = segue.destination as? BoltScannerController, let i = tableView.indexPathForSelectedRow {
                c.scanForLine(items[i.row]);
            }
        }
    }

    // MARK: - Data display

    /// Set the `Invoice` this controller is showing the lines of.
    func showInvoice(_ invoice: Invoice) {
        self.title = invoice.title;
        self.items = invoice.lines;
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LineCell", for: indexPath);
        let line = items[indexPath.row];

        cell.textLabel?.text = "\(line.manufacturer ?? "Unknown Manufacturer"), \(line.description)";

        return cell;
    }
}
