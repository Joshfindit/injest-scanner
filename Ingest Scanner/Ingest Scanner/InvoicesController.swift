//
//  InvoicesController.swift
//  Ingest Scanner
//
//  Created by Josh Buxton on 2018-10-24.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import UIKit
import Core

class InvoicesController: UITableViewController {

    private let online = OnlineController<JSONArrayResource<Invoice>>();

    private var status: StatusView!

    /// The currently displayed invoices in this controller.
    private var items: [Invoice] = [];

    override func viewDidLoad() {
        super.viewDidLoad();

        refreshControl = UIRefreshControl();
        refreshControl!.addTarget(online, action: #selector(online.start), for: .valueChanged)

        self.status = StatusView.createInstance();
        tableView.backgroundView = status;
        status.layer.zPosition = 10;
        online.status = status;
        self.tableView.scrollRectToVisible(CGRect.zero, animated: false);

        // setup the online controller
        online.loadContent = { [unowned self] callback in
            self.showStatus();
            self.refreshControl?.beginRefreshing();
            return Access.shared.getInvoices(callback);
        }

        online.showContent = { [unowned self] array in
            let count = self.items.count;
            self.items = [];
            self.tableView.deleteRows(at: (0..<count).map { IndexPath(row: $0, section: 0) }, with: .automatic)
            self.items = array.items ?? [];
            self.tableView.insertRows(at: (0..<self.items.count).map { IndexPath(row: $0, section: 0) }, with: .automatic)
            self.refreshControl?.endRefreshing();
            self.hideStatus();
        }

        online.start();
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LineSegue" {
            if let c = segue.destination as? LinesController, let s = tableView.indexPathForSelectedRow {
                c.showInvoice(items[s.row]);
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count;
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceCell", for: indexPath);

        cell.textLabel?.text = items[indexPath.row].title;

        return cell;
    }

    // MARK: - Status view

    /// Animate in the status view.
    private func showStatus() {
        UIView.animate(withDuration: 0.5, animations: {
            self.status.alpha = 1;
        });
    }

    /// Animate out the status view.
    private func hideStatus() {
        UIView.animate(withDuration: 0.5, animations: {
            self.status.alpha = 0;
        }, completion: { completed in
            if completed {
                self.status.isHidden = true;
            }
        });
    }
}
