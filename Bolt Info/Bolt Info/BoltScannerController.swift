//
//  BoltScannerController.swift
//  Bolt Info
//
//  Created by Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import Core

class BoltScannerController: ScannerController {

    /// The last scanned `CodeInfo`.
    private var info: CodeInfo?

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "InfoSegue" {
            if let c = segue.destination.childViewControllers.first as? BoltInfoController, let i = info {
                c.info = i;
            }
        }
    }

    // MARK: - Data display

    internal override func showInfo(_ info: CodeInfo) {
        self.info = info;
        self.performSegue(withIdentifier: "InfoSegue", sender: nil);
    }
}
