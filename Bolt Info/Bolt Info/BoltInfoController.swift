//
//  BoltInfoController.swift
//  Bolt Info
//
//  Created by Josh Buxton on 2018-10-13.
//  Copyright © 2018 Josh Buxton. All rights reserved.
//

import UIKit
import Core

class BoltInfoController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    private let online = OnlineController<Bolt>();
    private let categoriesSection = 4;

    private var status: StatusView!
    private var bolt: Bolt?

    @IBOutlet private weak var cameraButton: UIBarButtonItem!

    @IBOutlet private weak var thumbnailView: OnlineImageView!
    @IBOutlet private weak var postingPhotoIndicator: UIActivityIndicatorView!

    @IBOutlet weak var skuLabel: UILabel!
    @IBOutlet weak var stockedAmountLabel: UILabel!
    @IBOutlet weak var lengthLabel: UILabel!
    @IBOutlet weak var supplierLabel: UILabel!
    @IBOutlet weak var manufacturerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var ingestDateLabel: UILabel!

    @IBOutlet weak var invoiceNameLabel: UILabel!
    @IBOutlet weak var invoiceDateLabel: UILabel!
    @IBOutlet weak var invoiceLineLabel: UILabel!
    @IBOutlet weak var invoiceLineUUIDLabel: UILabel!
    @IBOutlet weak var originalCostLabel: UILabel!
    @IBOutlet weak var sellingPriceLabel: UILabel!
    @IBOutlet weak var landedCostLabel: UILabel!

    @IBOutlet weak var conceptualTagsLabel: UILabel!
    @IBOutlet weak var categoryTrackingTagsLabel: UILabel!

    /// The `CodeInfo` for this controller to get the bolt of.
    ///
    /// Must be set before the view of this controller is loaded.
    var info: ScannerController.CodeInfo?

    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }

    @IBAction func onCamera(_ sender: Any) {
        let picker = UIImagePickerController();

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera;
            picker.delegate = self;

            self.present(picker, animated: true, completion: nil);
        }
        else {
            let alert = UIAlertController(title: "Error", message: "Unable to detect a camera.", preferredStyle: .alert);
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil));

            self.present(alert, animated: true, completion: nil);
        }
    }

    @IBAction func onDeleteBolt(_ sender: Any) {
        guard let bolt = self.bolt else {
            return;
        }

        let alert = UIAlertController(title: "Confirm Delete Bolt", message: bolt.sku ?? "This is permanent: Bolt cannot be recovered once deleted.", preferredStyle: .alert);
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil));
        alert.addAction(UIAlertAction(title: "Confirm", style: .destructive, handler: { [unowned self] _ in
            if let uuid = self.bolt?.uuid {
                _ = Access.shared.deleteBolt(uuid: uuid, { (response) in
                    var message: String;

                    do {
                        let message = try response.unwrap();
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Bolt Successfully Deleted", message: message.message, preferredStyle: .alert);
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                                self.dismiss(animated: true, completion: nil);
                            }));

                            self.present(alert, animated: true, completion: nil);
                        }
                    }
                    catch let e {
                        let ne = e as NSError;
                        message = ne.localizedRecoverySuggestion ?? ne.localizedDescription;
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert);
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: nil));
                            self.present(alert, animated: true, completion: nil);
                            // alert two with message of message
                        }
                    }
                });
            }
        }));


        self.present(alert, animated: true, completion: nil);
    }


    override func viewDidLoad() {
        super.viewDidLoad();

        // setup the status view
        status = StatusView.createInstance();
        status.layer.zPosition = 10;

        tableView.backgroundView = status;

        // setup the online controller
        online.status = status;

        online.loadContent = { [unowned self] callback in
            if let u = self.info?.uuid {
                self.showStatus();
                return Access.shared.getBolt(uuid: u, callback);
            }

            return nil;
        }

        online.showContent = { [unowned self] bolt in
            self.hideStatus();
            self.cameraButton.isEnabled = true;
            self.showBolt(bolt);
        }

        // start the online controller
        online.start();
    }

    // MARK: - Data display

    private func showBolt(_ bolt: Bolt) {
        self.bolt = bolt;
        self.thumbnailView.url = bolt.mediumThumbnail;

        let doubleFormatter = NumberFormatter();
        doubleFormatter.maximumFractionDigits = 2;

        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "YYYY-MM-dd";

        let currencyFormatter = NumberFormatter();
        currencyFormatter.numberStyle = .currency;

        self.skuLabel.text = bolt.sku;

        if let sa = bolt.stockedAmount {
            self.stockedAmountLabel.text = String(sa);
        }
        else {
            self.stockedAmountLabel.text = "-";
        }


        self.lengthLabel.text = doubleFormatter.string(from: bolt.length as NSNumber);
        self.supplierLabel.text = bolt.supplier;
        self.manufacturerLabel.text = bolt.manufacturer;
        self.descriptionLabel.text = bolt.description;
        self.uuidLabel.text = bolt.uuid;
        self.ingestDateLabel.text = dateFormatter.string(from: bolt.ingestDate);

        self.invoiceNameLabel.text = bolt.invoiceTitle;
        self.invoiceDateLabel.text = bolt.invoiceDate;
        self.invoiceLineLabel.text = bolt.invoiceLineTitle;
        self.invoiceLineUUIDLabel.text = bolt.invoiceLineUUID;

        self.originalCostLabel.text = currencyFormatter.string(from: bolt.originalCost as NSNumber);
        self.sellingPriceLabel.text = currencyFormatter.string(from: bolt.sellingPrice as NSNumber);
        self.landedCostLabel.text = currencyFormatter.string(from: bolt.landedCost as NSNumber);

        self.conceptualTagsLabel.text = bolt.conceptualTags; // conceptual_tags=>["Metallic"]
        self.categoryTrackingTagsLabel.text = bolt.categoryTrackingTags;

        self.tableView.reloadData();
    }

    // MARK: - Picker controller delegate

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let i = info[UIImagePickerControllerOriginalImage] as? UIImage, let b = bolt {
            postingPhotoIndicator.startAnimating();

            UIView.animate(withDuration: 0.5, animations: {
                self.postingPhotoIndicator.alpha = 1;
            });

            _ = Access.shared.postBoltImage(i, for: b, { [weak self] response in
                do {
                    let bolt = try response.unwrap();

                    DispatchQueue.main.async {
                        self?.showBolt(bolt);
                    }
                }
                catch let e {
                    print("Error uploading bolt image: \(e)");

                    let ne = e as NSError;
                    let alert = UIAlertController(title: "Error uploading bolt image", message: ne.localizedRecoverySuggestion ?? ne.localizedDescription, preferredStyle: .alert);
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil));

                    DispatchQueue.main.async {
                        self?.present(alert, animated: true, completion: nil);
                    }
                }

                if let strongSelf = self {
                    DispatchQueue.main.async {
                        UIView.animate(withDuration: 0.5, animations: {
                            strongSelf.postingPhotoIndicator.alpha = 0;
                        }, completion: { completed in
                            if completed {
                                strongSelf.postingPhotoIndicator.stopAnimating();
                            }
                        });
                    }
                }
            });
        }

        self.presentedViewController?.dismiss(animated: true, completion: nil);
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return (bolt != nil && bolt!.categories.count > 0) ? categoriesSection + 1 : categoriesSection + 2 ;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == categoriesSection {
            return bolt?.categories.count ?? 0;
        }

        return super.tableView(tableView, numberOfRowsInSection: section);
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == categoriesSection {
            let cell = UITableViewCell(style: .default, reuseIdentifier: nil);

            cell.textLabel?.text = bolt?.categories[indexPath.row];

            return cell;
        }

        return super.tableView(tableView, cellForRowAt: indexPath);
    }

    // MARK: - Table view delegate

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            // make the thumbnail view row resize to a 256:171 aspect ratio to fit the standard thumbnail aspect ratio
            return tableView.bounds.width * (171 / 256);
        }
        else if indexPath.section == categoriesSection {
            return 44;
        }

        return super.tableView(tableView, heightForRowAt: indexPath);
    }

    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        return 0;
    }

    // MARK: - Status view

    /// Animate in the status view.
    private func showStatus() {
        UIView.animate(withDuration: 0.5, animations: {
            self.status.alpha = 1;
        });
    }

    /// Animate out the status view.
    private func hideStatus() {
        UIView.animate(withDuration: 0.5, animations: {
            self.status.alpha = 0;
        }, completion: { completed in
            if completed {
                self.status.isHidden = true;
            }
        });
    }
}
